import { useDispatch } from "react-redux"
import { loadAllProducts } from "../store/thunk/product_thunk"

export default function ButtonLoad() {
  const dispatch = useDispatch()

  const loadProducts = () => {
    dispatch(loadAllProducts())
  }
  return (
    <button onClick={loadProducts}>Load Products</button>
  )
}