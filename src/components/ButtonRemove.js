import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { removeProduct } from "../store/reducers/product"
export default function ButtonRemove() {
  const products = useSelector(state => state.product.items)
  const dispatch = useDispatch()

  useEffect(() => {
    console.log(products)
  }, [products])

  const removeOldProduct = () => {
    dispatch(removeProduct(1))
  }

  return (
    <button onClick={removeOldProduct}>Remove Product</button>
  )
}