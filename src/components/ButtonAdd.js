import { useState } from "react"
import { useDispatch } from "react-redux"
import { addProduct } from "../store/reducers/product"

export default function ButtonAdd() {
  const dispatch = useDispatch()

  const addNewProduct = () => {
    dispatch(addProduct({
      id: 1,
      title: "Laptop"
    }))
  }

  return (
    <button onClick={addNewProduct}>Add product</button>
  )
}

// export async function getServerSideProps() {
//   console.log('button add')
//   return {
//     props: {}
//   }
// }