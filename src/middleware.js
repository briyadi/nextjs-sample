import { NextResponse } from "next/server"

// This function can be marked `async` if using `await` inside
export function middleware(request) {
    if (request.nextUrl.pathname.startsWith("/dynamic")) {
        return NextResponse.redirect(new URL('/login', request.url))
    }
    // ...
    //   return NextResponse.redirect(new URL('/login', request.url))
    console.log("cuma mampir saja...")
    return NextResponse.next()
}

// See "Matching Paths" below to learn more
export const config = {
    matcher: "/about/:path*",
}
