import { useRouter } from "next/router"

export default function AboutDetail() {
    const router = useRouter()
    const {year, month, day, test} = router.query
    return (
        <>
            <h1>AboutDetail {year}/{month}/{day} {test}</h1>
        </>
    )
}