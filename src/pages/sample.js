import { useEffect } from "react"

export default function Sample({products}) {

  useEffect(() => {
    console.log(`sample page`, products)
  }, [])

  return (
    <>
      <h1>Sample page</h1>
    </>
  )
}

export async function getStaticProps() {
  let products = []
  console.log('request to server ...')
  try {
      let res = await fetch("https://dummyjson.com/products")
      let data = await res.json()
      products = [...data.products]
  } catch (e) {}

  return {
      props: {products}
  }
}