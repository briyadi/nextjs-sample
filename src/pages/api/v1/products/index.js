export default async function ProductIndex(req, res) {
    if (req.method === "POST") {
        res.status(201).json({
            message: "created"
        })
        return
    }
    
    res.json([
        {
            id: 1,
            name: "Mobile phone"
        },
        {
            id: 2,
            name: "Laptop"
        }
    ])
}