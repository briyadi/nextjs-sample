import { useEffect } from "react"
import { useSelector } from "react-redux"
import ButtonAdd from "../components/ButtonAdd"
import ButtonLoad from "../components/ButtonLoad"
import ButtonRemove from "../components/ButtonRemove"

// http://localhost:3000/
export default function Index({products}) {
    // const products = useSelector(state => state.product.items)

    useEffect(() => {
        console.log('list products', products)
    }, [])

    return (
        <>
            <h1>Hello world</h1>
            <ButtonAdd />
            <ButtonRemove />
            <ButtonLoad />
        </>
    )
}

export async function getServerSideProps() {
    let products = []
    console.log('request to server ...')
    try {
        let res = await fetch("https://dummyjson.com/products")
        let data = await res.json()
        products = [...data.products]
    } catch (e) {}

    return {
        props: {products}
    }
}