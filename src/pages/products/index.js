import { useEffect } from "react"
import ButtonAdd from "../../components/ButtonAdd"

// https://localhost:3000/products
export default function ProductIndex(props) {
    let {products = []} = props

    const getProducts = async () => {
        try {
            let res = await fetch("https://dummyjson.com/products")
            let data = await res.json()
            products = [...data.products]
            console.log('ini dari CSR', products)
        } catch (e) {
            console.log(e.message)
        }
    }

    useEffect(() => {
        getProducts()
    }, [])

    return (
        <>
            <h1>List products</h1>
            <ul>
                {products.map(p => {
                    return <li key={p.id}>{p.title}</li>
                })}
            </ul>
        </>
    )
}

export async function getServerSideProps() {
    let products = []
    try {
        let res = await fetch("https://dummyjson.com/products")
        let data = await res.json()
        products = [...data.products]
        console.log(products)
    } catch (e) {
        console.log(e.message)
    }

    return {
        props: {products}
    }
}