import { useRouter } from "next/router"

// https://localhost:3000/products/{id}
export default function GetProductById(props) {
    const router = useRouter()
    const { id } = router.query
    return (
        <>
            <h1>Detail {id}</h1>
        </>
    )
}