import Link from "next/link"
import { useRouter } from "next/router"

export default function DynamicRoutes() {
    const router = useRouter()
    const {slug} = router.query
    
    return (
        <>
            {slug}
            <button onClick={() => router.push("/about")}>
                Go to about page
            </button>
            <Link href="/about">Go to about page</Link>
        </>
    )
}