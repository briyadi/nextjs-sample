import { createSlice } from "@reduxjs/toolkit";
import { loadAllProducts } from "../thunk/product_thunk";

const slice = createSlice({
  name: "product",
  initialState: {
    items: []
  },
  reducers: {
    addProduct: (state, action) => {
      state.items.push(action.payload)
    },
    removeProduct: (state, action) => {
      let newProducts = state.items.filter(item => {
        return item.id !== action.payload
      })
      state.items = [...newProducts]
    }
  },
  extraReducers: builder => {
    builder.addCase(loadAllProducts.fulfilled, (state, action) => {
      state.items = [...action.payload]
    })

    builder.addCase(loadAllProducts.pending, (state, action) => {
      console.log('request to server ...')
    })

    builder.addCase(loadAllProducts.rejected, (state, action) => {
      console.log('error', action.error)
    })
  }
})

export const {addProduct, removeProduct} = slice.actions

export default slice.reducer