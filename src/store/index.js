import { configureStore } from "@reduxjs/toolkit";
import { createWrapper } from "next-redux-wrapper";
import product from "./reducers/product";

export const store = configureStore({
  reducer: {
    product
  }
})

const makeStore = () => store

export const wrapper = createWrapper(makeStore)