import { createAsyncThunk } from "@reduxjs/toolkit";

export const loadAllProducts = createAsyncThunk(
  'product/load',
  async () => {
    let res = await fetch("https://dummyjson.com/products")
    let data = await res.json()
    return data.products
  }
)